/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @since    27/08/2018
* @date     08/09/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <bitset>
using std::bitset;

#include <cstring>
using std::strlen;

#include "s_des.h"
#include "manip_arq.h"

/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[]) 
{
    // Faz a validação dos argumentos passados de acordo com a opção desejada
    if (argc != 5 && argc != 4) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensage,.txt" << endl;

        exit(1);
    }

    string opcao = argv[1];

    if (opcao != "cifrar" && opcao != "decifrar") {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    } else if (opcao == "cifrar" && argc != 5) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    } else if (opcao == "decifrar" && argc != 4) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    }

    if (strlen(argv[2]) != 10) {
        cerr << "--> Chave invalida !" << endl;
        cerr << "A chave deve ser uma sequencia de 10 bits" << endl;
        exit (1);
    }

    string chave_str = argv[2];
    bitset<10> chave_bit (chave_str);

    bitset<8> k1;
    bitset<8> k2;

    gera_k1k2 (chave_bit, k1, k2);

    bitset<8> k[2] = {k1, k2};

    string texto = ler_msg(argv[3]);

    // cout << "Texto: " << texto << endl;

    // Verifica a opção do usuario para cifrar ou decifrar
    if (opcao == "cifrar") {
        string texto_encrip = "";
        // Cifra um caracter por vez
        for (int ii = 0; ii < (int) texto.size(); ii++) {
            bitset<8> temp;
            temp = func_complexa (bitset<8>(texto[ii]), k, 1, true);

            texto_encrip += char (temp.to_ulong());
        }

        // cout << "Texto encriptado: " << texto_encrip << endl;
        cout << "--> Mensagem cifrada com sucesso !" << endl;

        gravar_msg(texto_encrip, argv[4]);
    } else {
        string texto_desencrip = "";
        // Decifra um caracter por vez
        for (int ii = 0; ii < (int) texto.size(); ii++) {
            bitset<8> temp;
            temp = func_complexa (bitset<8>(texto[ii]), k, 2, false);

            texto_desencrip += char (temp.to_ulong());
        }

        // cout << "Texto desencriptado: " << texto_desencrip << endl;
        cout << "--> Mensagem decifrada com sucesso !" << endl << endl;
        cout << texto_desencrip << endl;
    }

    return 0;
}
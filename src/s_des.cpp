/**
* @file	    s_des.cpp
* @brief	Implementação das funções que fazem a cifragem e decifragem utilizando 
            o algoritmo Simple DES
* @author   Pedro Emerick (p.emerick@live.com)
* @since    27/08/2018
* @date     08/09/2018
*/

#include "s_des.h"

#include <bitset>
using std::bitset;

#include <string>
using std::string;

/** 
 * @brief	Função que gera as chaves k1 e k2 do SDES através da chave
 * @param 	chave Chave para geração de chaves
 * @param   k1 Receberá a chave k1 gerada 
 * @param   k2 Receberá a chave k2 gerada
 */
void gera_k1k2 (bitset<10> chave, bitset<8>&k1, bitset<8>&k2) {
    int P10[10] = {3,5,2,7,4,10,1,9,8,6};
    int P8[8] = {6,3,7,4,8,5,10,9};
    
    string perm_ini = "";

    // Primeira permutação
    for (int ii = 0; ii < 10; ii++) {
        perm_ini += chave.to_string()[P10[ii]-1];
    }

    string temp = "";

    // Divisao em 2 partes
    for (int ii = 0; ii < 5; ii++) {
        temp += perm_ini[ii];
    }

    bitset<5> part1 (temp);

    temp = "";
    for (int ii = 5; ii < 10; ii++) {
        temp += perm_ini[ii];
    }

    bitset<5> part2 (temp);

    // Rotações a esquerda
    rotacao_esq (part1, 1);
    rotacao_esq (part2, 1);

    // K1
    temp = "";
    for (int ii = 0; ii < 8; ii++) {
        temp += (part1.to_string() + part2.to_string())[P8[ii]-1];
    }

    k1 = bitset<8>(temp);

    // Rotações a esquerda
    rotacao_esq(part1, 2);
    rotacao_esq(part2, 2);

    // K2
    temp = "";
    for (int ii = 0; ii < 8; ii++) {
        temp += (part1.to_string() + part2.to_string())[P8[ii]-1];
    }

    k2 = bitset<8>(temp);
}

/** 
 * @brief	Função que representa o componente complexo do SDES, chamada de função complexa Fk
 * @param 	texto_bit Bits a serem cifrados/decifrados
 * @param   k Vetor com as chaves k1 e k2 geradas
 * @param   num_k Representa qual chave será usado na chamada da função se será k1 (1) ou k2 (2)
 * @param   encriptar Booleano que diz se o bit será cifrado (true) ou decifrado (false)
 */
bitset<8> func_complexa (bitset<8> texto_bit, bitset<8> k[2], int num_k, bool encriptar) 
{
    int IP[8] = {2,6,3,1,4,8,5,7};
    int EP[8] = {4,1,2,3,2,3,4,1};
    int S0[4][4] = {{1,0,3,2}, {3,2,1,0}, {0,2,1,3}, {3,1,3,2}};
    int S1[4][4] = {{1,1,2,3}, {2,0,1,3}, {3,0,1,0}, {2,1,0,3}};
    int P4[4] = {2,4,3,1};
    int IP_1[8] = {4,1,3,5,7,2,8,6};

    string temp = "";

    if ((num_k == 1 && encriptar) || (num_k == 2 && !encriptar)) {
        // Permutação inicial
        for (int ii = 0; ii < 8; ii++) {
            temp += texto_bit.to_string()[IP[ii]-1];
        }

        texto_bit = bitset<8>(temp);
    } 

    // Separação de lado esquerdo e direito
    bitset<4> texto_esq_bit;
    bitset<4> texto_dir_bit;

    dividir_meio (texto_bit, texto_esq_bit, texto_dir_bit);

    // Permutação EP
    temp = "";
    for (int ii = 0; ii < 8; ii++) {
        temp += texto_dir_bit.to_string()[EP[ii]-1];
    }

    bitset<8> texto_ep_bit (temp);

    // XOR com chave gerada
    bitset<8> r_bit = (texto_ep_bit ^= k[num_k - 1]);

    bitset<4> r_esq_bit;
    bitset<4> r_dir_bit;

    dividir_meio(r_bit, r_esq_bit, r_dir_bit);

    // Caixa S0
    temp = "";
    temp += r_esq_bit.to_string()[0];
    temp += r_esq_bit.to_string()[3];

    int linha = bitset<2>(temp).to_ulong();
    
    temp = "";
    temp += r_esq_bit.to_string()[1];
    temp += r_esq_bit.to_string()[2];

    int coluna = bitset<2>(temp).to_ulong();

    bitset<2> r_s0 (S0[linha][coluna]);

    // Caixa S1
    temp = "";
    temp += r_dir_bit.to_string()[0];
    temp += r_dir_bit.to_string()[3];

    linha = bitset<2>(temp).to_ulong();
    
    temp = "";
    temp += r_dir_bit.to_string()[1];
    temp += r_dir_bit.to_string()[2];

    coluna = bitset<2>(temp).to_ulong();

    bitset<2> r_s1 (S1[linha][coluna]);

    // Permutação P4
    temp = "";
    for (int ii = 0; ii < 4; ii++) {
        temp += (r_s0.to_string() + r_s1.to_string())[P4[ii] - 1];
    }

    bitset <4> s0_s1_p4 (temp);

    // XOR com parte esquerda inicial
    texto_esq_bit = (s0_s1_p4 ^= texto_esq_bit);

    if ((num_k == 1 && encriptar) || (num_k == 2 && !encriptar)) {
        // Switch (SW)
        bitset<8> sw (texto_dir_bit.to_string() + texto_esq_bit.to_string());

        if (encriptar) {
            return func_complexa (sw, k, 2, true);
        } else {
            return func_complexa (sw, k, 1, false);
        }
    } else {
        // Junta os bits da esquerda com os da direita que passaram pelo processo
        bitset<8> final (texto_esq_bit.to_string() + texto_dir_bit.to_string());

        // Permutação Final
        temp = "";
        for (int ii = 0; ii < 8; ii++) {
            temp += final.to_string()[IP_1[ii]-1];
        }

        final = bitset<8>(temp);

        // Salva o arquivo no fim
        return final;
    }
}
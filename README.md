## SIMPLE DES (S-DES)

Este repositório contém um programa um que permite cifrar e decifrar uma mensagem de um arquivo seguindo o [S-DES](http://homepage.smc.edu/morgan_david/vpn/assignments/assgt-sdes-encrypt-sample.htm).

---

### Considerações Gerais:

O programa permite cifrar e decifrar mensagens com o Simples DES, utilizando como alfabeto toda a tabela ASCII.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila o programa;
* "make doc" = gera a documentação do programa, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para cifrar:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./prog cifrar chave mensagem.txt mensagem_saida.txt";
    * cifrar = diz para o programa que você deseja cifrar uma mensagem
    * chave = chave escolhida para cifragem, lembre-se que o S-DES usa uma chave de 10 bits
    * mensagem.txt = arquivo que contém a mensagem a ser cifrada
    * mensagem_saida.txt = arquivo em que a mensagem cifrada deve ser armazenada
* O programa irá cifrar a mensagem contida no arquivo passado como entrada, e salvará a mensagem cifrada no arquivo passado como saída.

---

### Para decifrar:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./prog decifrar chave mensagem.txt";
    * decifrar = diz para o programa que você deseja decifrar uma mensagem
    * chave = chave escolhida para decifragem, lembre-se que o S-DES usa uma chave de 10 bits
    * mensagem.txt = arquivo que contém a mensagem a ser decifrada
* A mensagem decifrada será impressa na saída padrão do programa.




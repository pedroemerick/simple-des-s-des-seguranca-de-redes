LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

RM = rm -rf
RM_TUDO = rm -fr

PROG = sdes

.PHONY: all clean debug doc doxygen gnuplot init valgrind

all: init $(PROG)

debug: CFLAGS += -g -O0
debug: $(PROG)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

$(PROG): $(OBJ_DIR)/main.o $(OBJ_DIR)/manip_arq.o $(OBJ_DIR)/s_des.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/manip_arq.h $(INC_DIR)/s_des.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/manip_arq.o: $(SRC_DIR)/manip_arq.cpp $(INC_DIR)/manip_arq.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/s_des.o: $(SRC_DIR)/s_des.cpp $(INC_DIR)/s_des.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes -v ./bin/multimat 2 4 8 16 32

doxygen:
	doxygen -g

doc:
	@mkdir -p $(DOC_DIR)/
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*

/**
* @file	    s_des.h
* @brief	Declaração do prototipo e implementação das funções que fazem a cifragem 
            e decifragem utilizando o algoritmo Simple DES
* @author   Pedro Emerick (p.emerick@live.com)
* @since    27/08/2018
* @date     08/09/2018
*/

#ifndef S_DES_H
#define S_DES_H

#include <bitset>
using std::bitset;

#include <string>
using std::string;

/** 
 * @brief	Função generica que faz a rotação a esquerda de bits
 * @param 	bit Bits que sofrerão a rotação
 * @param   rotate Quantidade de rotações a esquerda que ocorrerão
 */
template <size_t T>
void rotacao_esq (bitset<T> &bit, int rotate) {
    bit = bit << rotate | bit >> (T - rotate);
} 

/** 
 * @brief	Função generica dividi uma sequencia de bits em duas partes,
 *          dividindo ao meio
 * @param 	bit Bits que será dividido
 * @param   part1 Receberá os bits da esquerda   
 * @param   part2 Receberá os bits da direita
 */
template <size_t T>
void dividir_meio (bitset<T> bit, bitset<T/2> &part1, bitset<T/2> &part2) {
    string temp = "";
    int total = T;

    for (int ii = 0; ii < (total / 2); ii++) {
        temp += bit.to_string()[ii];
    }

    part1 = bitset<T/2> (temp);

    temp = "";

    for (int ii = (total / 2); ii < total; ii++) {
        temp += bit.to_string()[ii];
    }

    part2 = bitset<T/2> (temp);
}

/** 
 * @brief	Função que gera as chaves k1 e k2 do SDES através da chave
 * @param 	chave Chave para geração de chaves
 * @param   k1 Receberá a chave k1 gerada 
 * @param   k2 Receberá a chave k2 gerada
 */
void gera_k1k2 (bitset<10> chave, bitset<8>&k1, bitset<8>&k2);

/** 
 * @brief	Função que representa o componente complexo do SDES, chamada de função complexa Fk
 * @param 	texto_bit Bits a serem cifrados/decifrados
 * @param   k Vetor com as chaves k1 e k2 geradas
 * @param   num_k Representa qual chave será usado na chamada da função se será k1 (1) ou k2 (2)
 * @param   encriptar Booleano que diz se o bit será cifrado (true) ou decifrado (false)
 */
bitset<8> func_complexa (bitset<8> texto_bit, bitset<8> k[2], int num_k, bool encriptar);

#endif 